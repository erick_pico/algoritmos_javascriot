"use strict";

/**
 * saber si un numero ingresado es primo o no
 */
var numero = 20;

var primo = function primo(numero) {
  var contador = 0;

  for (var index = 2; index < numero; index++) {
    if (numero % index == 0) contador++;
  }

  if (contador > 1) {
    return numero + ' no es primo';
  } else {
    return numero + ' Es primo';
  }
};

module.exports = primo;