/**
 * ingresar un numero de 2 digitos y decir si sus digitos son pares
 */

 let numero = 45

const digitosPares = (numero) =>{
    if(numero <10 || numero >99) return ('ingrese un numero de 2 digitos');
    
        let n1 = parseInt(numero /10)
        let n2 = numero % 10
        if(n1%2 == 0 && n2%2== 0){
            return('ambos digitos son pares')
        }else{
            if(n1%2!=0){
               return(n1+' es impar')
            }else{
               return(n2+' es impar')
            }
        }
    
}
module.exports = digitosPares