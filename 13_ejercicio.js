/**
 * generar todas las tablas de multuplicar del 1 al 10
 */

let tbs = []
const tablas = ()=>{
    for (let i = 1; i <=10; i++) {
        for (let j = 1; j <=10; j++) {
         
            tbs.push(`${i} x ${j} = ${i*j}`)
        }
        
    }
    return tbs
}
module.exports = tablas