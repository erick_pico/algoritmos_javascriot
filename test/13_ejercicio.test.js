let eje13 = require('../13_ejercicio')

test('Regresa las tablas de multiplicar', () => {
    let res = eje13()
    expect(res).toContain('3 x 6 = 18')
});