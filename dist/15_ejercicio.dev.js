"use strict";

/**
 * * leer 10 numeros enteros, meterlos en un vector y decir cual es el mayor
 */
var lista = [];

for (var i = 1; i <= 10; i++) {
  lista.push(i);
}

var elMayor = function elMayor(lista) {
  var mayor = 0;

  for (var _i = 0; _i < lista.length; _i++) {
    var element = lista[_i];
    if (mayor < element) mayor = element;
  }

  return "el numero mayor es ".concat(mayor);
};

module.exports = elMayor;