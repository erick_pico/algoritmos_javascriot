"use strict";

/**
 * * leer 10 numeros enteros, meterlos en un vector y decir donde estan los que tienen 0
 */
var lista = [1, 20, 33, 17, 68, 30, 100, 341, 404, 10];

var enterosConCero = function enterosConCero(lista) {
  var result = [];

  for (var i = 0; i < lista.length; i++) {
    var element = lista[i];
    if (element % 10 == 0) result.push("el numero ".concat(element, " esta en ").concat(i, "\n"));
  }

  return result;
};

module.exports = enterosConCero;