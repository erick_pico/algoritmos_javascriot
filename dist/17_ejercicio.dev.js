"use strict";

/**
 * * leer 10 numeros enteros, meterlos en un vector y decir cual es el mayor
 */
var lista = [1, 20, 33,, 404, 17, 68, 30, 100, 341, 404, 10];

var elMayor = function elMayor(lista) {
  var mayor = 0;
  var contador = 0;

  for (var i = 0; i < lista.length; i++) {
    var element = lista[i];
    if (mayor < element) mayor = element;
  }

  for (var _i = 0; _i < lista.length; _i++) {
    var _element = lista[_i];
    if (_element == mayor) contador++;
  }

  return "el numero mayor es ".concat(mayor, " y aparece ").concat(contador);
};

module.exports = elMayor;