let eje17 = require('../17_ejercicio')
let lista = [1,20,33,404,17,68,30,100,341,404,10];
test('Regresa 404 y aparece 2 vez', () => {
    let res = eje17(lista)
    expect(res).toEqual('el numero mayor es 404 y aparece 2')
});

test('no mostrara que 20 es el mayor', () => {
    let res = eje17(lista)
    expect(res).not.toContain('el numero mayor es 20 y aparece 1')
});