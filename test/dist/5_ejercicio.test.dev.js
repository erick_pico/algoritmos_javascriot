"use strict";

var eje5 = require('../5_ejercicio');

test('Regresa si un numero es negativo', function () {
  var res = eje5(-2);
  expect(res).toContain('negativo');
});
test('Regresa si un numero primo', function () {
  var res = eje5(7);
  expect(res).toContain('primo');
});
test('Regresa si un numero es negativo', function () {
  var res = eje5(4);
  expect(res).toContain('no es');
});