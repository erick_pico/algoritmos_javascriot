/**
 * ejercicio para saber si un numero es par o impar
 */

let numero = 33
const pares = (numero)=>{
    if (numero % 2 == 0) {
       return(numero + ' es par')
    } else {
        return(numero + ' es impar')
    }
}
module.exports = pares