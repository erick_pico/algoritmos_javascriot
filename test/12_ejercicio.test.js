let eje12 = require('../12_ejercicio')

test('Regresa numero de veces que se repite el 1', () => {
    let res = eje12(14646212711)
    expect(res).toContain(4)
});