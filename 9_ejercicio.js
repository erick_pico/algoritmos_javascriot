/**
 * leer 3 numeros de 2 digitos y decir si el ultimo numero de los 3 son iguales
 */

let numero1 = 23
let numero2 = 33
let numero3 = 73

const tresNumeros = (numero1,numero2,numero3)=>{
    if((numero1 <10 || numero1 >99)||(numero2 <10 || numero2 >99)||(numero3 <10 || numero3 >99)){
        return('ingrese un numero de 2 digitos');
    }else{
        let n1 = numero1%10
        let n2 = numero2%10
        let n3 = numero3%10
        if(n1==n2 && n1==n3) return('son iguales')
        else return('no son iguales')
    }
}
module.exports = tresNumeros