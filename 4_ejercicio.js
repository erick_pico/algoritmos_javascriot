/**
 * saber si un numero ingresado es primo o no
 */
let numero = 20

const primo = (numero)=>{
    let contador = 0
    for (let index = 2; index < numero; index++) {
        if(numero%index == 0) contador++
    }

    if (contador>1) {
       return(numero + ' no es primo')
    }else{
        return(numero+' Es primo')
    }
}
module.exports = primo