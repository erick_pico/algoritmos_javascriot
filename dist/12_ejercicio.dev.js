"use strict";

/**
 * leer un numero y saber cuantas veces contiene el 1
 */
var numero = 1221431;

var contadorUno = function contadorUno(numero) {
  var numero_list = numero.toString().split('');
  var contador = 0;
  numero_list.forEach(function (element) {
    if (parseInt(element) == 1) contador++;
  });
  return "el numero 1 aparece ".concat(contador, " veces");
};

module.exports = contadorUno;