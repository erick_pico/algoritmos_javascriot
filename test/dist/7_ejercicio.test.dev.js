"use strict";

var eje7 = require('../7_ejercicio');

test('Regresa mensaje de error', function () {
  var res = eje7(100);
  expect(res).toContain('ingrese');
});
test('Regresa mensaje diciendo que son iguales', function () {
  var res = eje7(11);
  expect(res).toEqual('Los numeros son iguales');
});
test('Regresa mensaje diciendo que no son iguales', function () {
  var res = eje7(12);
  expect(res).toEqual('No son iguales');
});