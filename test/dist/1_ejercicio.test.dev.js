"use strict";

var eje1 = require('../1_ejercicio');

test('Regresa un mensaje diciendo que el numero es par', function () {
  var res = eje1(2);
  expect(res).toContain('par');
});
test('Regresa otro mensaje diferente de par', function () {
  var res = eje1(3);
  expect(res).toEqual('3 es impar');
});