let eje9 = require('../9_ejercicio')

test('Regresa mensaje de error', () => {
    let res = eje9(12,34,998)
    expect(res).toContain('ingrese un')
});

test('Regresa son iguales', () => {
    let res = eje9(12,12,12)
    expect(res).toEqual('son iguales')
});
test('Regresa no son iguales', () => {
    let res = eje9(12,21,12)
    expect(res).toEqual('no son iguales')
});