"use strict";

var eje8 = require('../8_ejercicio');

test('Regresa mensaje de error', function () {
  var res = eje8(2);
  expect(res).toContain('ingrese');
});
test('Regresa mensaje si no son iguales', function () {
  var res = eje8(1234);
  expect(res).toEqual('No son iguales los numeros');
});
test('Regresa mensaje si son iguales', function () {
  var res = eje8(1223);
  expect(res).toEqual('Los numeros son iguales');
});