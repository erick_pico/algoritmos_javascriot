/**
 * leer un entero de 2 digitos y calcular su suma
 */

let numero = 45
const suma = (numero)=> {
    if(numero <10 || numero >99){
       return('ingrese un numero de 2 digitos');
    }else{
        let n1 = parseInt(numero /10)
        let n2 = numero % 10
        let res = n1+n2
        return(res)
    }
}
module.exports = suma