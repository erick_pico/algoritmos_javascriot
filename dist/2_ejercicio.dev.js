"use strict";

/**
 * leer un entero de 2 digitos y calcular su suma
 */
var numero = 45;

var suma = function suma(numero) {
  if (numero < 10 || numero > 99) {
    return 'ingrese un numero de 2 digitos';
  } else {
    var n1 = parseInt(numero / 10);
    var n2 = numero % 10;
    var res = n1 + n2;
    return res;
  }
};

module.exports = suma;