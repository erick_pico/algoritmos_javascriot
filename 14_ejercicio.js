/**
 ** leer un entero y generar su tabla de multiplicar 
 *? (no se si es hasta un punto determinado (del 1 al 10 con ese numero) o hasta ese numero(del 1 al n))
 */

 let numero = 23
 let resultado = []
const generateTable = (numero)=>{
    for (let i = 1; i <=10; i++) {
        resultado.push(`${numero} x ${i} = ${i*numero}`)
       
   }
   return resultado
}

module.exports = generateTable