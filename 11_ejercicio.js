/**
 * leer un numero y determinar cuanto es la suma de sus digitos
 */

 let numero = 7776
 const sumaTotal = (numero)=>{
    let numero_cast = numero.toString()
    let res = 0
    numero_cast = numero_cast.split('')
   
    numero_cast.forEach((element) => {
        res += parseInt(element)
    });
   return('la suma es: '+res)
 }
 module.exports = sumaTotal