"use strict";

/**
 * saber si un numero ingresado es primo o no y ademas si es negativo
 */
var numero = -20;
var contador = 0;

var primoNeg = function primoNeg(numero) {
  if (numero < 0) {
    numero = numero * -1;
    return numero + ' Es negativo';
  }

  for (var index = 2; index <= numero; index++) {
    if (numero % index == 0) contador++;
  }

  if (contador > 1) {
    return numero + ' no es primo';
  } else {
    return numero + ' Es primo';
  }
};

module.exports = primoNeg;