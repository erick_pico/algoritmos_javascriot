/**
 * ingresar un numero de dos digitos y decir si son iguales
 */

let numero = 45
const equals = (numero)=>{
    if(numero <10 || numero >99){
        return('ingrese un numero de 2 digitos');
    }else{
        let n1 = parseInt(numero /10)
        let n2 = numero % 10
        if(n1==n2)   return('Los numeros son iguales')
        else return('No son iguales')
    }
}
module.exports = equals