"use strict";

var eje15 = require('../15_ejercicio');

var numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
test('Regresa como numero mayor 10', function () {
  var res = eje15(numeros);
  expect(res).toContain('10');
});
test('No regresa 9 como numero mayor', function () {
  var res = eje15(numeros);
  expect(res).not.toContain('9');
});