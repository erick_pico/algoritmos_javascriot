/**
 * leer un numero y saber cuantas veces contiene el 1
 */

let numero = 1221431;
const contadorUno = (numero)=>{
    let numero_list = numero.toString().split('')
    let contador = 0
    numero_list.forEach(element => {
        if(parseInt(element)==1) contador++
    });

   return(`el numero 1 aparece ${contador} veces`)
}
module.exports = contadorUno