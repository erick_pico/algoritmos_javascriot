"use strict";

var eje3 = require('../3_ejercicio');

test('Regresa ambos pares', function () {
  var res = eje3(22);
  expect(res).toContain('ambos digitos');
});
test('Regresa algun numero es impar', function () {
  var res = eje3(12);
  expect(res).toContain('impar');
});
test('Regresa mensaje de error', function () {
  var res = eje3(120);
  expect(res).toContain('ingrese un numero');
});