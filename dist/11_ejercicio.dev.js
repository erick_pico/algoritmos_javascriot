"use strict";

/**
 * leer un numero y determinar cuanto es la suma de sus digitos
 */
var numero = 7776;

var sumaTotal = function sumaTotal(numero) {
  var numero_cast = numero.toString();
  var res = 0;
  numero_cast = numero_cast.split('');
  numero_cast.forEach(function (element) {
    res += parseInt(element);
  });
  return 'la suma es: ' + res;
};

module.exports = sumaTotal;