"use strict";

var eje3 = require('../4_ejercicio');

test('Regresa si un numero es primo', function () {
  var res = eje3(21);
  expect(res).toContain('es primo');
});
test('Regresa si un numero no es primo', function () {
  var res = eje3(20);
  expect(res).toContain('no es');
});