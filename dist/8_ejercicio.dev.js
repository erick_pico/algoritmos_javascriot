"use strict";

/**
 * leer un numero de 4 digitos y decir si el segundo numero es igual al penultimo
 */
var numero = 1232;

var penultimo = function penultimo(numero) {
  var numero_cast = numero.toString();
  numero_cast = numero_cast.split('');

  if (numero < 1000 || numero > 9999) {
    return 'ingrese un numero de 4 digitos';
  } else {
    if (numero_cast[1] == numero_cast[2]) return 'Los numeros son iguales';else return 'No son iguales los numeros';
  }
};

module.exports = penultimo;