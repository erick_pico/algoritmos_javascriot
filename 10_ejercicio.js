/**
 * * comparar 2 numeros y verificar si su diferencia es divisor de alguno de los 2
 */

let numero1 = 23
let numero2 = 78
const divisor = (numero1, numero2)=>{
    let res = numero1 - numero2

    if(numero1%res == 0){
        return(`${res} es divisor de ${numero1}`)
    }else if(numero2%res == 0){
        return(`${res} es divisor de ${numero2}`)
    }else{
        return(res+ " no es divisor de ninguno")
    }
}
module.exports = divisor