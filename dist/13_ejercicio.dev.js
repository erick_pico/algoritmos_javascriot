"use strict";

/**
 * generar todas las tablas de multuplicar del 1 al 10
 */
var tbs = [];

var tablas = function tablas() {
  for (var i = 1; i <= 10; i++) {
    for (var j = 1; j <= 10; j++) {
      tbs.push("".concat(i, " x ").concat(j, " = ").concat(i * j));
    }
  }

  return tbs;
};

module.exports = tablas;