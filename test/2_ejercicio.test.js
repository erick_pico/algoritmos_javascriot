let eje2 = require('../2_ejercicio')

test('Regresa la suma de un entero de 2 digitos', () => {
    let res = eje2(22)
    expect(res).toEqual(4)
});

test('Regresa un mensaje de error', () => {
    let res = eje2(222)
    expect(res).toContain('ingrese un numero')
});