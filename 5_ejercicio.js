/**
 * saber si un numero ingresado es primo o no y ademas si es negativo
 */
 let numero = -20
 let contador = 0
 const primoNeg= (numero)=>{
    if(numero<0){
        numero = numero *(-1)
        return(numero+' Es negativo')
    }
    for (let index = 2; index <= numero; index++) {
        if(numero%index == 0) contador++
    }
    
    if (contador>1) {
        return(numero + ' no es primo')
    }else{
        return(numero+' Es primo')
    }
 }
 module.exports = primoNeg