let eje3 = require('../3_ejercicio')

test('Regresa ambos pares', () => {
    let res = eje3(22)
    expect(res).toContain('ambos digitos')
});

test('Regresa algun numero es impar', () => {
    let res = eje3(12)
    expect(res).toContain('impar')
});

test('Regresa mensaje de error', () => {
    let res = eje3(120)
    expect(res).toContain('ingrese un numero')
});