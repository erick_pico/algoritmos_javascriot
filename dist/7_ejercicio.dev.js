"use strict";

/**
 * ingresar un numero de dos digitos y decir si son iguales
 */
var numero = 45;

var equals = function equals(numero) {
  if (numero < 10 || numero > 99) {
    return 'ingrese un numero de 2 digitos';
  } else {
    var n1 = parseInt(numero / 10);
    var n2 = numero % 10;
    if (n1 == n2) return 'Los numeros son iguales';else return 'No son iguales';
  }
};

module.exports = equals;