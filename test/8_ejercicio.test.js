let eje8 = require('../8_ejercicio')

test('Regresa mensaje de error', () => {
    let res = eje8(2)
    expect(res).toContain('ingrese')
});
test('Regresa mensaje si no son iguales', () => {
    let res = eje8(1234)
    expect(res).toEqual('No son iguales los numeros')
});
test('Regresa mensaje si son iguales', () => {
    let res = eje8(1223)
    expect(res).toEqual('Los numeros son iguales')
});