"use strict";

var eje11 = require('../11_ejercicio');

test('Regresa la suma total', function () {
  var res = eje11(1234);
  expect(res).toContain(10);
});