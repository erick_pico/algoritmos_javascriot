/**
 * * leer 10 numeros enteros, meterlos en un vector y decir cual es el mayor
 */

let lista = [];
for (let i = 1; i <= 10; i++) {
    lista.push(i)
}
const elMayor = (lista) =>{
    let mayor = 0
    for (let i = 0; i < lista.length; i++) {
        const element = lista[i];
        if (mayor < element) mayor = element
    }
    return(`el numero mayor es ${mayor}`)
}
module.exports = elMayor