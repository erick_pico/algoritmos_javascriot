"use strict";

/**
 ** leer un entero y generar su tabla de multiplicar 
 *? (no se si es hasta un punto determinado (del 1 al 10 con ese numero) o hasta ese numero(del 1 al n))
 */
var numero = 23;
var resultado = [];

var generateTable = function generateTable(numero) {
  for (var i = 1; i <= 10; i++) {
    resultado.push("".concat(numero, " x ").concat(i, " = ").concat(i * numero));
  }

  return resultado;
};

module.exports = generateTable;