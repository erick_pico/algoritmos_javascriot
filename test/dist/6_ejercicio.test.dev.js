"use strict";

var eje6 = require('../6_ejercicio');

test('Regresar un mensaje de error', function () {
  var res = eje6(4444);
  expect(res).toContain('ingrese');
});
test('Regresar el segundo numero es multiplo del primero', function () {
  var res = eje6(48);
  expect(res).toEqual('4 es multiplo de 8');
});
test('Regresar el primero numero es multiplo del segundo', function () {
  var res = eje6(42);
  expect(res).toEqual('2 es multiplo de 4');
});