"use strict";

/**
 * leer 3 numeros de 2 digitos y decir si el ultimo numero de los 3 son iguales
 */
var numero1 = 23;
var numero2 = 33;
var numero3 = 73;

var tresNumeros = function tresNumeros(numero1, numero2, numero3) {
  if (numero1 < 10 || numero1 > 99 || numero2 < 10 || numero2 > 99 || numero3 < 10 || numero3 > 99) {
    return 'ingrese un numero de 2 digitos';
  } else {
    var n1 = numero1 % 10;
    var n2 = numero2 % 10;
    var n3 = numero3 % 10;
    if (n1 == n2 && n1 == n3) return 'son iguales';else return 'no son iguales';
  }
};

module.exports = tresNumeros;