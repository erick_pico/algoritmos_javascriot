let eje10 = require('../10_ejercicio')

test('Regresa si es divisor de alguno de los 2 numeros', () => {
    let res = eje10(12,6)
    expect(res).toContain('es divisor')
});

test('Regresa que no es divisor de ninguno', () => {
    let res = eje10(20,1)
    expect(res).toContain('no es divisor')
});