let eje15 = require('../15_ejercicio')
let numeros = [1,2,3,4,5,6,7,8,9,10]
test('Regresa como numero mayor 10', () => {
    let res = eje15(numeros)
    expect(res).toContain('10')
});

test('No regresa 9 como numero mayor', () => {
    let res = eje15(numeros)
    expect(res).not.toContain('9')
});