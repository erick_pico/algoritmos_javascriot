/**
 * * leer 10 numeros enteros, meterlos en un vector y decir donde estan los que tienen 0
 */

 let lista = [1,20,33,17,68,30,100,341,404,10];
 
 let enterosConCero = (lista) =>{
     let result = []
    for (let i = 0; i < lista.length; i++) {
        const element = lista[i];
        if(element%10==0)
        result.push(`el numero ${element} esta en ${i}\n`)
        
    }
    return result

 }
 module.exports = enterosConCero