/**
 * * leer 10 numeros enteros, meterlos en un vector y decir cual es el mayor
 */

 let lista = [1,20,33,,404,17,68,30,100,341,404,10];
 const elMayor = (lista) => {
    let mayor = 0
    let contador = 0
    for (let i = 0; i < lista.length; i++) {
        const element = lista[i];
        if (mayor < element) mayor = element
    }
    for (let i = 0; i < lista.length; i++) {
        const element = lista[i];
        if(element == mayor) contador++
        
    }
    return(`el numero mayor es ${mayor} y aparece ${contador}`)
 }

 module.exports = elMayor