"use strict";

var eje16 = require('../16_ejercicio');

var lista = [1, 20, 33, 17, 68, 30, 100, 341, 404, 10];
test('Regresa una lista con 20,30,100,10 ', function () {
  var res = eje16(lista);
  expect(res[0]).toContain('20');
});
test('No mostrara 404', function () {
  var res = eje16(lista);
  expect(res).not.toContain('404');
});