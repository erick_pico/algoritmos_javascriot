"use strict";

var eje14 = require('../14_ejercicio');

test('Regresa una tabla generada deacuerdo el numero', function () {
  var res = eje14(43);
  expect(res).toContain('43 x 2 = 86');
});