"use strict";

/**
 * ejercicio para saber si un numero es par o impar
 */
var numero = 33;

var pares = function pares(numero) {
  if (numero % 2 == 0) {
    return numero + ' es par';
  } else {
    return numero + ' es impar';
  }
};

module.exports = pares;