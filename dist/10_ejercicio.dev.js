"use strict";

/**
 * * comparar 2 numeros y verificar si su diferencia es divisor de alguno de los 2
 */
var numero1 = 23;
var numero2 = 78;

var divisor = function divisor(numero1, numero2) {
  var res = numero1 - numero2;

  if (numero1 % res == 0) {
    return "".concat(res, " es divisor de ").concat(numero1);
  } else if (numero2 % res == 0) {
    return "".concat(res, " es divisor de ").concat(numero2);
  } else {
    return res + " no es divisor de ninguno";
  }
};

module.exports = divisor;