 /**
 * ingresar 2 numero y saber si son multiplos
 */

 let numero = 48
 const multiplos = (numero) => {
    if(numero <10 || numero >99){
        return('ingrese un numero de 2 digitos');
    }else{
        let n1 = parseInt(numero /10)
        let n2 = numero % 10
       
        for (i = 0 ; i <n1 ; i++){
            if(n2*i == n1){
                return(n2+' es multiplo de ' + n1)
            }
        }
        for (i = 0 ; i <n2 ; i++){
           if(n1*i == n2){
               return(n1+' es multiplo de ' + n2)
           }
       }
    }
 }
 module.exports = multiplos